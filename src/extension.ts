// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';


// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

	vscode.commands.registerTextEditorCommand('vestjysk-marketing.translationString', function (editor, edit, args) {


		const activeEditor = vscode.window.activeTextEditor;

		if (activeEditor) {
			//For Getting File Path
			let filePath = activeEditor.document.uri.path;
			let folders = filePath.split("/");

			let plugin: string = '';

			folders.forEach((folder, index) => {

				if (!plugin) {
					if (folder == 'src') {
						plugin = "app";
					}
					if (folder == 'plugins') {
						plugin = folders[index + 1];
					}

				}
			});

			plugin = plugin.replace(/(?<!^)(?=[A-Z])/g, '_').toLocaleLowerCase();

			let text = "__d('" + plugin + "', '$0')";
			editor.insertSnippet(new vscode.SnippetString(text));
		}

	});
}

// this method is called when your extension is deactivated
export function deactivate() { }
